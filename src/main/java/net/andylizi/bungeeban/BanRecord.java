/*
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package net.andylizi.bungeeban;

import net.md_5.bungee.api.connection.ProxiedPlayer;

import java.io.Serializable;
import java.util.Date;
import java.util.Objects;

import static net.andylizi.bungeeban.Utils.DATE_FORMAT;

/**
 * 封禁记录.
 *
 * @author andylizi
 */
public class BanRecord implements Serializable {
    private static final long serialVersionUID = 1L;

    /** 记录唯一ID. */
    public final int id;

    /** 被封禁者用户名. 全小写. */
    public final String username;

    /** 操作者. */
    public final String operator;

    /** 操作时间. */
    public final Date time;

    /** 封禁理由. */
    public final String reason;

    /** 封禁截止时间. */
    public final Date expire;

    /**
     * @param id       记录ID
     * @param username 用户名将被自动转换为小写
     * @param operator 操作者
     * @param reason   理由
     * @param expire   封禁截止期, null为永久
     */
    public BanRecord(int id, String username, String operator, Date time, String reason, Date expire) {
        this.id = id;
        this.username = Objects.requireNonNull(username).toLowerCase();
        this.operator = operator;
        this.time = time;
        this.reason = reason;
        this.expire = expire;
    }

    public String getKickMessage() {
        StringBuilder builder = new StringBuilder()
                .append("§c您的帐号已被")
                .append(canExpire() ? "临时" : "永久")
                .append("封禁, ");
        if (reason != null) {
            builder.append("原因: \n§6").append(reason);
        }
        if (canExpire()) {
            builder.append("\n§c封禁将于 §e§o").append(DATE_FORMAT.format(expire)).append(" §c自动解除");
        }
        if (Config.tips != null) {
            builder.append("\n§r").append(Config.tips);
        }
        return builder.toString();
    }

    public void kick(ProxiedPlayer player) {
        player.disconnect(Utils.toTextComponent(getKickMessage()));
    }

    public boolean canExpire() {
        return expire != null;
    }

    public boolean checkExpired() {
        return canExpire() && expire.after(new Date());
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) return true;
        if (obj == null) return false;
        if (getClass() != obj.getClass()) return false;
        final BanRecord other = (BanRecord) obj;
        return this.id == other.id &&
                Objects.equals(this.username, other.username) &&
                Objects.equals(this.time, other.time);
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 83 * hash + this.id;
        hash = 83 * hash + Objects.hashCode(this.username);
        hash = 83 * hash + Objects.hashCode(this.time);
        return hash;
    }
}
