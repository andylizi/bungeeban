/*
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package net.andylizi.bungeeban;

import net.andylizi.bungeeban.commands.CommandBan;
import net.andylizi.bungeeban.commands.CommandBanInfo;
import net.andylizi.bungeeban.commands.CommandUnban;
import net.andylizi.bungeeban.database.IDatabase;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.chat.BaseComponent;
import net.md_5.bungee.api.event.PostLoginEvent;
import net.md_5.bungee.api.plugin.Listener;
import net.md_5.bungee.api.plugin.Plugin;
import net.md_5.bungee.event.EventHandler;
import net.md_5.bungee.event.EventPriority;

import java.util.concurrent.RejectedExecutionException;
import java.util.concurrent.TimeUnit;

/**
 * @author andylizi
 */
public final class BungeeBan extends Plugin implements Listener {
    public final Config config;
    public IDatabase database;

    public BungeeBan() {
        config = new Config(this);
    }

    @Override
    public void onEnable() {
        try {
            config.loadConfig();
        } catch (Throwable ex) {
            throw new RuntimeException(ex);
        }
        getProxy().getPluginManager().registerListener(this, this);
        getProxy().getPluginManager().registerCommand(this, new CommandBan(this));
        getProxy().getPluginManager().registerCommand(this, new CommandUnban(this));
        getProxy().getPluginManager().registerCommand(this, new CommandBanInfo(this));
    }

    @Override
    public void onDisable() {
        try {
            if (database instanceof AutoCloseable)
                ((AutoCloseable) database).close();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public static void broadcast(String msg, CommandSender... senders) {
        BaseComponent[] bmsg = Utils.toBaseComponents(msg);

        CommandSender consoleSender = ProxyServer.getInstance().getConsole();
        consoleSender.sendMessage(bmsg);
        for (CommandSender sender : senders)
            if (sender != consoleSender)
                sender.sendMessage(bmsg);
    }

    @EventHandler(priority = EventPriority.HIGHEST)
    public void onJoin(PostLoginEvent event) {
        try {
            database.checkBanned(event.getPlayer().getName(), record -> {
                if (record == null)
                    return;
                getProxy().getScheduler().schedule(this, () ->
                        record.kick(event.getPlayer()), 1, TimeUnit.SECONDS);
            });
        } catch (RejectedExecutionException ex) {
            event.getPlayer().disconnect(Utils.toTextComponent("§c抱歉, 系统繁忙, 请稍后再试"));
        }
    }
}
