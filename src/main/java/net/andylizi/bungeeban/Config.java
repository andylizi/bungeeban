/*
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package net.andylizi.bungeeban;

import com.google.common.util.concurrent.ThreadFactoryBuilder;
import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;
import net.andylizi.bungeeban.database.MySqlDatabase;
import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.config.Configuration;
import net.md_5.bungee.config.ConfigurationProvider;
import net.md_5.bungee.config.YamlConfiguration;

import java.io.File;
import java.nio.file.Files;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.function.Supplier;

/**
 * @author andylizi
 */
public class Config {
    final BungeeBan plugin;

    public static String tips;
    public static int waitingTime;

    public Config(BungeeBan plugin) {
        this.plugin = plugin;
    }

    public void loadConfig() throws Throwable {
        if (!plugin.getDataFolder().exists())
            plugin.getDataFolder().mkdirs();
        File file = new File(plugin.getDataFolder(), "config.yml");
        if (!file.exists()) {
            Files.copy(plugin.getResourceAsStream(file.getName()), file.toPath());
        }
        Configuration config = ConfigurationProvider.getProvider(YamlConfiguration.class).load(file);
        loadBanSection(config.getSection("ban"));
        loadDatabase(config.getSection("database"));
    }

    private void loadBanSection(Configuration config) {
        tips = ChatColor.translateAlternateColorCodes('&', config.getString("additionalTips", ""));
        if (tips.trim().isEmpty())
            tips = null;
        waitingTime = config.getInt("waitingTime");
    }

    @SuppressWarnings("SpellCheckingInspection")
    private void loadDatabase(Configuration config) throws Throwable {
        Supplier<ExecutorService> executorSupplier = () ->
                // new ThreadPoolExecutor(0, 2048,
                //         1, TimeUnit.MINUTES,
                //         new ArrayBlockingQueue<>(6),
                //         new ThreadFactoryBuilder()
                //                 .setNameFormat("BungeeBan-Database-%d")
                //                 .setDaemon(true)
                //                 .build(),
                //         new ThreadPoolExecutor.AbortPolicy());
                plugin.getProxy().getScheduler().unsafe().getExecutorService(plugin);

        HikariConfig dbConfig = new HikariConfig();
        dbConfig.setThreadFactory(new ThreadFactoryBuilder()
                .setThreadFactory(new GroupThreadFactory(new ThreadGroup("BungeeBan-ConnectionPool")))
                .setNameFormat("BungeeBan-ConnectionPool-%d")
                .setDaemon(true)
                .build());
        dbConfig.setPoolName("BungeeBan-Database");
        dbConfig.setRegisterMbeans(true);

        dbConfig.setJdbcUrl(String.format("jdbc:mysql://%s:%s/%s",
                config.getString("address"), config.getInt("port"), config.getString("databaseName")));
        dbConfig.setDriverClassName("com.mysql.jdbc.Driver");
        dbConfig.setUsername(config.getString("username"));
        dbConfig.setPassword(config.getString("password"));

        dbConfig.addDataSourceProperty("useSSL", "false");
        dbConfig.addDataSourceProperty("cachePrepStmts", "true");
        dbConfig.addDataSourceProperty("useServerPrepStmts", "true");
        dbConfig.addDataSourceProperty("prepStmtCacheSize", "250");
        dbConfig.addDataSourceProperty("prepStmtCacheSqlLimit", "2048");
        dbConfig.addDataSourceProperty("cacheResultSetMetadata", "true");
        dbConfig.addDataSourceProperty("elideSetAutoCommits", "true");
        dbConfig.addDataSourceProperty("maintainTimeStats", "true");

        plugin.database = new MySqlDatabase(executorSupplier,
                new HikariDataSource(dbConfig),
                config.getString("table"));
    }
}
