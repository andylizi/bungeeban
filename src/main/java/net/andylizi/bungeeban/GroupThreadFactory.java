package net.andylizi.bungeeban;

import java.util.concurrent.ThreadFactory;

/**
 * @author andylizi
 */
public class GroupThreadFactory implements ThreadFactory{
    protected final ThreadGroup group;

    public GroupThreadFactory(ThreadGroup group) {
        this.group = group;
    }

    @Override
    public Thread newThread(Runnable r) {
        return new Thread(group, r);
    }
}