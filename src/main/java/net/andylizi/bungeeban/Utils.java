/*
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package net.andylizi.bungeeban;

import net.md_5.bungee.api.chat.BaseComponent;
import net.md_5.bungee.api.chat.TextComponent;

import java.text.DateFormat;
import java.text.FieldPosition;
import java.text.ParsePosition;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Locale;
import java.util.concurrent.TimeUnit;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @author andylizi
 */
public final class Utils {
    @SuppressWarnings("SpellCheckingInspection")
    public static final DateFormat DATE_FORMAT = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.CHINA);
    public static final DateFormat DIFF_DATE_FORMATER = new DiffDateFormater();

    public static BaseComponent[] toBaseComponents(String str) {
        return TextComponent.fromLegacyText(str);
    }

    public static TextComponent toTextComponent(String str) {
        return new TextComponent(toBaseComponents(str));
    }

    private Utils() throws AssertionError { throw new AssertionError(); }

    private static class DiffDateFormater extends DateFormat {
        private final long MINUTE = TimeUnit.SECONDS.convert(1, TimeUnit.MINUTES);
        private final long HOUR = TimeUnit.SECONDS.convert(1, TimeUnit.HOURS);
        private final long DAY = TimeUnit.SECONDS.convert(1, TimeUnit.DAYS);

        @Override
        public StringBuffer format(Date date, StringBuffer builder, FieldPosition fieldPosition) {
            long diff = TimeUnit.MILLISECONDS.toSeconds(date.getTime()) -
                    TimeUnit.MILLISECONDS.toSeconds(System.currentTimeMillis());
            if (diff == 0) {
                builder.append("0s");
                return builder;
            } else if (diff < 0) {
                builder.append('-');
                diff = Math.abs(diff);
            }

            if (diff > DAY) {
                builder.append(diff / DAY).append('d');
                diff %= DAY;
            }
            if (diff > HOUR) {
                builder.append(diff / HOUR).append('h');
                diff %= HOUR;
            }
            if (diff > MINUTE) {
                builder.append(diff / MINUTE).append('m');
                diff %= MINUTE;
            }
            if (diff > 0)
                builder.append(diff).append('s');

            return builder;
        }

        final Pattern TIME_PARSE_PATTERN = Pattern.compile("(?:([0-9]+)\\s*y[a-z]*[,\\s]*)?(?:([0-9]+)\\s*mo[a-z]*[,\\s]*)?(?:([0-9]+)\\s*w[a-z]*[,\\s]*)?(?:([0-9]+)\\s*d[a-z]*[,\\s]*)?(?:([0-9]+)\\s*h[a-z]*[,\\s]*)?(?:([0-9]+)\\s*m[a-z]*[,\\s]*)?(?:([0-9]+)\\s*(?:s[a-z]*)?)?", Pattern.CASE_INSENSITIVE);

        @Override
        @SuppressWarnings("ConstantConditions")
        public Date parse(String source, ParsePosition pos) {
            pos.setIndex(source.length());
            int years = 0, months = 0, weeks = 0, days = 0, hours = 0, minutes = 0, seconds = 0;
            try {
                days = Integer.parseInt(source);
            } catch (NumberFormatException ex) {
                Matcher matcher = TIME_PARSE_PATTERN.matcher(source);
                boolean found = false;
                while (matcher.find()) {
                    if ((matcher.group() != null) && (!matcher.group().isEmpty())) {
                        for (int i = 0; i < matcher.groupCount(); i++) {
                            if ((matcher.group(i) != null) && (!matcher.group(i).isEmpty())) {
                                found = true;
                                break;
                            }
                        }
                        if (found) {
                            if ((matcher.group(1) != null) && (!matcher.group(1).isEmpty()))
                                years = Integer.parseInt(matcher.group(1));
                            if ((matcher.group(2) != null) && (!matcher.group(2).isEmpty()))
                                months = Integer.parseInt(matcher.group(2));
                            if ((matcher.group(3) != null) && (!matcher.group(3).isEmpty()))
                                weeks = Integer.parseInt(matcher.group(3));
                            if ((matcher.group(4) != null) && (!matcher.group(4).isEmpty()))
                                days = Integer.parseInt(matcher.group(4));
                            if ((matcher.group(5) != null) && (!matcher.group(5).isEmpty()))
                                hours = Integer.parseInt(matcher.group(5));
                            if ((matcher.group(6) != null) && (!matcher.group(6).isEmpty()))
                                minutes = Integer.parseInt(matcher.group(6));
                            if ((matcher.group(7) != null) && (!matcher.group(7).isEmpty()))
                                seconds = Integer.parseInt(matcher.group(7));
                        }
                    }
                }
                if (!found) {
                    pos.setIndex(0);
                    pos.setErrorIndex(0);
                    return null;
                }
            }

            calendar = new GregorianCalendar();
            if (years > 0)
                calendar.add(Calendar.YEAR, years);
            if (months > 0)
                calendar.add(Calendar.MONTH, months);
            if (weeks > 0)
                calendar.add(Calendar.WEEK_OF_YEAR, weeks);
            if (days > 0)
                calendar.add(Calendar.DATE, days);
            if (hours > 0)
                calendar.add(Calendar.HOUR_OF_DAY, hours);
            if (minutes > 0)
                calendar.add(Calendar.MINUTE, minutes);
            if (seconds > 0)
                calendar.add(Calendar.SECOND, seconds);
            return calendar.getTime();
        }
    }
}
