/*
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package net.andylizi.bungeeban.commands;

import net.andylizi.bungeeban.BanRecord;
import net.andylizi.bungeeban.BungeeBan;
import net.andylizi.bungeeban.Utils;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Command;

import java.text.ParseException;
import java.util.Date;
import java.util.concurrent.Future;
import java.util.concurrent.RejectedExecutionException;
import java.util.concurrent.TimeUnit;

/**
 * @author andylizi
 */
public class CommandBan extends Command {
    private final BungeeBan plugin;

    @SuppressWarnings("SpellCheckingInspection")
    public CommandBan(BungeeBan plugin) {
        super("bcban", "bungeeban.ban");
        this.plugin = plugin;
    }

    @Override
    public void execute(CommandSender sender, String[] args) {
        if (args.length < 1 || args[0].trim().isEmpty()) {
            sender.sendMessage(TextComponent.fromLegacyText("§c参数不足, 请使用 §6§o/bcban <Player> [Reason] [Time]"));
            return;
        }
        String username = args[0], operator = sender.getName(), reason = null;
        Date expire = null;
        if (args.length > 1 && !args[1].trim().isEmpty())
            reason = args[1];
        if (args.length > 2 && !args[2].trim().isEmpty()) {
            try {
                expire = Utils.DIFF_DATE_FORMATER.parse(args[2]);
            } catch (ParseException ignored) { }
            if (expire == null) {
                sender.sendMessage(TextComponent.fromLegacyText("§c无效的时间格式. e.g: 3d"));
                return;
            }
        }

        StringBuilder msg = new StringBuilder()
                .append(new StringBuilder().append("§e管理员 §6")
                        .append(operator).append(" §e封禁了 §6")
                        .append(username).append(" §e,原因: §6").append(reason == null ? "无" : reason));
        if (expire != null)
            msg.append("§e,时间: §6").append(Utils.DIFF_DATE_FORMATER.format(expire));

        BanRecord record = new BanRecord(-1, username, operator, null, reason, expire);
        try {
            Future<Boolean> task = plugin.database.ban(record, result -> {
                if (result) {
                    BungeeBan.broadcast(msg.toString(), sender);

                    ProxiedPlayer player;
                    if ((player = plugin.getProxy().getPlayer(username)) != null) {
                        record.kick(player);
                    }
                } else sender.sendMessage(Utils.toTextComponent("§c封禁操作执行失败!"));
            });
            plugin.getProxy().getScheduler().schedule(plugin, () -> {
                if (!task.isDone()) {
                    sender.sendMessage(Utils.toTextComponent("§e正在更新封禁数据..."));
                }
            }, 1, TimeUnit.SECONDS);
        } catch (RejectedExecutionException ex) {
            sender.sendMessage(Utils.toTextComponent("§c系统繁忙, 请稍后重试..."));
        }
    }
}
