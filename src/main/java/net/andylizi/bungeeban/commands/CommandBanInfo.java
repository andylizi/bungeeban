/*
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package net.andylizi.bungeeban.commands;

import net.andylizi.bungeeban.BanRecord;
import net.andylizi.bungeeban.BungeeBan;
import net.andylizi.bungeeban.Utils;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.plugin.Command;

import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;

/**
 * @author andylizi
 */
public class CommandBanInfo extends Command {
    private final BungeeBan plugin;

    @SuppressWarnings("SpellCheckingInspection")
    public CommandBanInfo(BungeeBan plugin) {
        super("bcbaninfo", "bungeeban.baninfo");
        this.plugin = plugin;
    }

    @Override
    public void execute(CommandSender sender, String[] args) {
        if (args.length < 1 || args[0].trim().isEmpty()) {
            sender.sendMessage(TextComponent.fromLegacyText("§c参数不足, 请使用 §6§o/bcbaninfo <Player>"));
            return;
        }
        Future<BanRecord> task = plugin.database.checkBanned(args[0], record -> {
            if (record == null) {
                sender.sendMessage(Utils.toTextComponent("§e没有有效记录"));
                return;
            }
            sender.sendMessage(Utils.toTextComponent(recordToString(record)));
        });
        plugin.getProxy().getScheduler().schedule(plugin, () -> {
            if (!task.isDone())
                sender.sendMessage(Utils.toTextComponent("§e查询中..."));
        }, 1, TimeUnit.SECONDS);
    }

    public static String recordToString(BanRecord record) {
        return String.format("%s §7-> §e操作者§6`%s` §e时间§6`%s` §e理由§6`%s` §e过期§6`%s`",
                record.username, record.operator, Utils.DATE_FORMAT.format(record.time), record.reason,
                record.canExpire() ? Utils.DATE_FORMAT.format(record.expire) : "否");
    }
}
