/*
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package net.andylizi.bungeeban.commands;

import net.andylizi.bungeeban.BungeeBan;
import net.andylizi.bungeeban.Utils;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.plugin.Command;

import java.util.concurrent.Future;
import java.util.concurrent.RejectedExecutionException;
import java.util.concurrent.TimeUnit;

/**
 * @author andylizi
 */
public class CommandUnban extends Command {
    private final BungeeBan plugin;

    @SuppressWarnings("SpellCheckingInspection")
    public CommandUnban(BungeeBan plugin) {
        super("bcunban", "bungeeban.unban");
        this.plugin = plugin;
    }

    @Override
    public void execute(CommandSender sender, String[] args) {
        if (args.length < 1 || args[0].trim().isEmpty()) {
            sender.sendMessage(TextComponent.fromLegacyText("§c参数不足, 请使用 §6§o/unban <Player>"));
            return;
        }
        String name = args[0].toLowerCase();
        try {
            Future<Boolean> task = plugin.database.unban(name, result -> {
                if (result) {
                    BungeeBan.broadcast("§e管理员 §6" + sender.getName() + " §e解封了 §6" + name, sender);
                } else sender.sendMessage(Utils.toTextComponent("§c玩家未被封禁或数据库更新失败"));
            });
            plugin.getProxy().getScheduler().schedule(plugin, () -> {
                if (!task.isDone()) {
                    sender.sendMessage(Utils.toTextComponent("§e正在更新封禁数据..."));
                }
            }, 1, TimeUnit.SECONDS);
        } catch (RejectedExecutionException ex) {
            sender.sendMessage(Utils.toTextComponent("§c系统繁忙, 请稍后重试..."));
        }
    }
}
