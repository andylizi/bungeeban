/*
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package net.andylizi.bungeeban.database;

import net.andylizi.bungeeban.BanRecord;

import java.io.Closeable;
import java.io.IOException;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Future;
import java.util.concurrent.FutureTask;
import java.util.concurrent.RejectedExecutionException;
import java.util.function.Consumer;
import java.util.function.Supplier;

/**
 * @author andylizi
 */
public abstract class ConcurrentDatabase implements IDatabase, Closeable {
    private ExecutorService executor;

    public ConcurrentDatabase(Supplier<ExecutorService> executor) {
        this.executor = executor != null ? executor.get() : null;
    }

    @Override
    public void close() throws IOException {
        if (executor != null) {
            synchronized (executor) {
                executor.shutdown();
                executor = null;
            }
        }
    }

    @Override
    public Future<Boolean> ban(BanRecord record, Consumer<Boolean> callback) throws RejectedExecutionException {
        return execute(() -> ban0(record), callback);
    }

    abstract boolean ban0(BanRecord record);

    @Override
    public Future<Boolean> unban(String username, Consumer<Boolean> callback) throws RejectedExecutionException {
        return execute(() -> unban0(username), callback);
    }

    abstract boolean unban0(String username);

    @Override
    public Future<Boolean> expire(int id, Consumer<Boolean> callback) throws RejectedExecutionException {
        return execute(() -> expire0(id), callback);
    }

    abstract boolean expire0(int id);

    @Override
    public Future<BanRecord> checkBanned(String username, Consumer<BanRecord> callback) throws RejectedExecutionException {
        return execute(() -> checkBanned0(username), callback);
    }

    abstract BanRecord checkBanned0(String username);

    @Override
    public Future<Map<String, BanRecord>> getBanList(Consumer<Map<String, BanRecord>> callback) throws RejectedExecutionException {
        return execute(this::getBanList0, callback);
    }

    public abstract Map<String, BanRecord> getBanList0();

    private <T> Future<T> execute(Supplier<T> task, final Consumer<T> callback) throws RejectedExecutionException {
        class Task {
            private final Supplier<T> supplier;
            private final Consumer<T> callback;

            public Task(Supplier<T> supplier, Consumer<T> callback) {
                this.callback = callback;
                this.supplier = supplier;
            }

            public T execute() {
                T result = null;
                try {
                    result = supplier.get();
                } catch (Exception ex) {
                    if (ex.getCause() != null)
                        ex.getCause().printStackTrace();
                    else ex.printStackTrace();
                } finally {
                    if (callback != null)
                        callback.accept(result);
                }
                return result;
            }
        }
        if (executor != null) {
            return executor.submit(new Task(task, callback)::execute);
        }
        @SuppressWarnings("unchecked")
        FutureTask<T> rs = new FutureTask(new Task(task, callback)::execute);
        rs.run();
        return rs;
    }
}
