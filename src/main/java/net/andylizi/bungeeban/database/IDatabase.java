/*
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package net.andylizi.bungeeban.database;

import net.andylizi.bungeeban.BanRecord;

import java.util.Map;
import java.util.concurrent.Future;
import java.util.concurrent.RejectedExecutionException;
import java.util.function.Consumer;

/**
 * @author andylizi
 */
public interface IDatabase {

    /**
     * 封禁玩家.
     */
    Future<Boolean> ban(BanRecord record, Consumer<Boolean> callback) throws RejectedExecutionException;

    default Future<Boolean> ban(BanRecord record) throws RejectedExecutionException {
        return ban(record, null);
    }

    /**
     * 解封玩家.
     *
     * @param username 用户名将被自动转换为小写
     */
    Future<Boolean> unban(String username, Consumer<Boolean> callback) throws RejectedExecutionException;

    default Future<Boolean> unban(String username) throws RejectedExecutionException {
        return unban(username, null);
    }

    Future<Boolean> expire(int id, Consumer<Boolean> callback) throws RejectedExecutionException;

    default Future<Boolean> expire(int id) throws RejectedExecutionException {
        return expire(id, null);
    }

    Future<BanRecord> checkBanned(String username, Consumer<BanRecord> callback) throws RejectedExecutionException;

    Future<Map<String, BanRecord>> getBanList(Consumer<Map<String, BanRecord>> callback) throws RejectedExecutionException;
}
