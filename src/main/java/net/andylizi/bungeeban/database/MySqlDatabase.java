/*
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package net.andylizi.bungeeban.database;

import com.zaxxer.hikari.HikariDataSource;
import net.andylizi.bungeeban.BanRecord;

import java.io.IOException;
import java.sql.*;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.function.Supplier;

/**
 * @author andylizi
 */
@SuppressWarnings("SpellCheckingInspection")
public class MySqlDatabase extends ConcurrentDatabase {
    private static final String SQL_CREATE_TABLE
            = "CREATE TABLE IF NOT EXISTS `%s` ("
            + "  `id` int unsigned NOT NULL AUTO_INCREMENT,"
            + "  `username` varchar(64) NOT NULL,"
            + "  `operator` varchar(255) DEFAULT NULL,"
            + "  `time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,"
            + "  `reason` varchar(255) DEFAULT NULL,"
            + "  `expire` datetime DEFAULT NULL,"
            + "  `hasExpired` bit(1) NOT NULL DEFAULT b'0',"
            + "  PRIMARY KEY (`id`),"
            + "  UNIQUE KEY `name` (`username`) USING BTREE"
            + ") ENGINE=InnoDB DEFAULT CHARSET=utf8;";
    private final HikariDataSource dataSource;
    private final String table;

    public MySqlDatabase(Supplier<ExecutorService> executor, HikariDataSource dataSource, String table) throws Throwable {
        super(executor);
        this.dataSource = dataSource;
        this.table = table;
        createTable();
    }

    @Override
    public void close() throws IOException {
        super.close();
        dataSource.close();
    }

    private void createTable() throws SQLException {
        try (Connection conn = dataSource.getConnection(); Statement statement = conn.createStatement()) {
            statement.executeUpdate(String.format(SQL_CREATE_TABLE, table));
        }
    }

    @Override
    boolean ban0(BanRecord record) throws RuntimeException {
        unban0(record.username);
        try (Connection conn = dataSource.getConnection(); PreparedStatement statement =
                conn.prepareStatement(String.format("INSERT INTO `%s` (`username`, `operator`, `time`, `reason`, `expire`) VALUES (?, ?, NOW(), ?, ?);", table))) {
            statement.setString(1, record.username);
            statement.setString(2, record.operator);
            statement.setString(3, record.reason);
            statement.setTimestamp(4, record.expire == null ? null :
                    new Timestamp(record.expire.getTime()));
            return statement.executeUpdate() > 0;
        } catch (SQLException ex) {
            throw new RuntimeException(ex);
        }
    }

    @Override
    boolean unban0(String username) throws RuntimeException {
        try (Connection conn = dataSource.getConnection();
             PreparedStatement statement = conn.prepareStatement(String.format("DELETE FROM `%s` WHERE `username` = ? LIMIT 1;", table))) {
            statement.setString(1, username.toLowerCase());
            return statement.executeUpdate() > 0;
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    boolean expire0(int id) {
        try (Connection conn = dataSource.getConnection();
             PreparedStatement statement = conn.prepareStatement(String.format("UPDATE `%s` SET `hasExpired` = b'1' "
                     + "AND `hasExpired` = 0 WHERE `id` = ? LIMIT 1;", table))) {
            statement.setInt(1, id);
            return statement.executeUpdate() > 0;
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    BanRecord checkBanned0(String username) throws RuntimeException {
        try (Connection conn = dataSource.getConnection();
             PreparedStatement statement = conn.prepareStatement(String.format("SELECT `id`,`operator`,`time`,`reason`,`expire` "
                     + "FROM `%s` WHERE `username` = ?  AND `hasExpired` = 0 LIMIT 1;", table))) {
            statement.setString(1, username.toLowerCase());
            try (ResultSet rs = statement.executeQuery()) {
                if (rs.next()) {
                    return parseBanRecord(username, rs);
                } else return null;
            }
        } catch (SQLException ex) {
            throw new RuntimeException(ex);
        }
    }

    @Override
    public Map<String, BanRecord> getBanList0() throws RuntimeException {
        throw new UnsupportedOperationException();
    }

    private BanRecord parseBanRecord(String name, ResultSet rs) throws SQLException {
        if (name == null)
            name = rs.getString("username");
        int id = rs.getInt("id");
        Timestamp expire = rs.getTimestamp("expire");
        if (expire != null) {
            if (new java.util.Date().after(expire)) {
                System.out.println(String.format("Player %s 's banning has been expired", name));
                expire(id);
                return null;
            }
        }
        return new BanRecord(id, name, rs.getString("operator"), rs.getTimestamp("time"), rs.getString("reason"), expire);
    }
}
