package net.andylizi.bungeeban;

import org.junit.Ignore;
import org.junit.Test;

import java.text.ParseException;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

import static net.andylizi.bungeeban.Utils.DIFF_DATE_FORMATER;
import static org.junit.Assert.assertEquals;

public class DiffDateFormaterTest {
    @Test
    public void testPositive() throws Exception {
        String str = "";
        Calendar calendar = new GregorianCalendar();

        str += "5d";
        calendar.add(Calendar.DATE, 5);
        assertCalendar(str, calendar);
        assertCalendar(calendar, str);
        assertCalendar(calendar, "5");

        str += "8h";
        calendar.add(Calendar.HOUR_OF_DAY, 8);
        assertCalendar(str, calendar);
        assertCalendar(calendar, str);

        str += "45m";
        calendar.add(Calendar.MINUTE, 45);
        assertCalendar(str, calendar);
        assertCalendar(calendar, str);

        str += "33s";
        calendar.add(Calendar.SECOND, 33);
        assertCalendar(str, calendar);
        assertCalendar(calendar, str);

        calendar = new GregorianCalendar();
        calendar.add(Calendar.DATE, -8);
        assertCalendar("-8d", calendar);
    }

    @Test
    public void testNegative() throws Exception {
        String str = "-";
        Calendar calendar = new GregorianCalendar();

        str += "8d";
        calendar.add(Calendar.DATE, -8);
        assertCalendar(str, calendar);

        str += "16h";
        calendar.add(Calendar.HOUR_OF_DAY, -16);
        assertCalendar(str, calendar);

        str += "9s";
        calendar.add(Calendar.SECOND, -9);
        assertCalendar(str, calendar);
    }

    @Test(expected = ParseException.class)
    public void testParseInvalid1() throws Exception {
        DIFF_DATE_FORMATER.parse("a");
    }

    @Ignore
    @Test(expected = ParseException.class)
    public void testParseInvalid2() throws Exception {
        DIFF_DATE_FORMATER.parse("1dys");
    }

    private static void assertCalendar(String expected, Calendar actual) {
        assertEquals(expected, DIFF_DATE_FORMATER.format(new Date(actual.getTimeInMillis())));
    }

    private static void assertCalendar(Calendar expected, String actual) throws ParseException {
        assertEquals(expected.getTimeInMillis(), DIFF_DATE_FORMATER.parse(actual).getTime(), 900);
    }
}